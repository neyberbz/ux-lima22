import Vue from 'vue'
import VueRouter from 'vue-router'
import {routes} from './routes'
// import VueClazyLoad from 'vue-clazy-load'
// import BootstrapVue from 'bootstrap-vue'
import SocialSharing from 'vue-social-sharing'
import VueHead from 'vue-head'
import VueAwesomeSwiper from 'vue-awesome-swiper'
import VueFilter from 'vue-filter'
import App from './App.vue'
Vue.use(VueHead)
// Vue.use(BootstrapVue)
Vue.use(SocialSharing)
Vue.use(VueFilter)
Vue.use(VueRouter)
Vue.use(VueAwesomeSwiper)
// Vue.use(VueClazyLoad)

const router = new VueRouter({
  mode: 'history',
  routes
})

new Vue({
  el: '#app',
  router,
  render: h => h(App)
})
